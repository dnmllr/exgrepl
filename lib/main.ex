defmodule ExGrepl do
  alias ExGrepl.Index
  alias ExGrepl.Scanner
  alias IO.ANSI

  def main(_args \\ []) do
    # start the index process
    Index.start_link()

    # start a supervisor for scanning and processing file tasks
    {:ok, supervisor} = Task.Supervisor.start_link

    # begin the cli
    run_cli(supervisor, MapSet.new())
  end

  defp run_cli(supervisor, extensions) do
    # retrieve command from stdin
    command = IO.gets("#{ANSI.green()}enter command#{ANSI.blue()}>#{ANSI.default_color()}")
              |> String.trim()

    # initial parsing of the command
    # Commands come in either nullary or unary forms
    # arg will be nil for nullary commands
    [command, arg] = if String.contains?(command, " ") do
      String.split(command, " ")
    else
      [command, nil]
    end

    # parse and dispatch the command
    case string_to_command(command) do
      # exit the loop
      :exit -> IO.puts("exiting...")

      # modify loop state
      :add_extension -> run_cli(supervisor, MapSet.put(extensions, arg))
      :remove_extension -> run_cli(supervisor, MapSet.delete(extensions, arg))

      # seperate the common case of commands which merely read state
      # and then loop
      command_ ->
        case command_ do
          :index -> Scanner.index(supervisor, extensions, arg)
          :query -> IO.puts(Index.query(arg))
          :inspect -> Index.inspect()
          :unknown -> IO.puts "Command not recognized"
          :help -> print_help()

          # this should be unreachable
          _ -> raise ArgumentError, "Unknown command added #{inspect(command_)}"
        end

        # loop
        run_cli(supervisor, extensions)
    end
  end

  defp string_to_command(str) do
    # look at all the user friendliness! Aliases and shortcuts! Case insensitivity!
    # Wow!
    case String.upcase(str) do
      x when x in ["EXIT", "E"] -> :exit
      x when x in ["ADDEXT", "ADDEXTENSION", "ADD_EXTENSION", "A"] -> :add_extension
      x when x in ["REMEXT", "REMOVEEXT",
                   "REMOVEEXTENSION", "REMOVE_EXT",
                   "REMOVE_EXTENSION", "R"] -> :remove_extension
      x when x in ["INDEX", "I"] -> :index
      x when x in ["QUERY", "Q"] -> :query
      x when x in ["INSPECT", "INSP"] -> :inspect
      x when x in ["HELP", "H"] -> :help
      _ -> :unknown
    end
  end

  defp print_help do
    # TODO(Dan) write help
    IO.puts """
    Help to be written someday...
    """
  end
end
