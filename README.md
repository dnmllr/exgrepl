# exgrepl

## Getting Started

* [install elixir](https://elixir-lang.org/install.html)
* `git clone git@gitlab.com:dnmllr/exgrepl.git`
* `cd exgrepl`
* `mix deps.get`
* `MIX_ENV=prod mix escript.build`
* `./exgrepl`
