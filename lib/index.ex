defmodule ExGrepl.Index do
  use GenServer
  alias IO.ANSI

  ##
  # TODO(Dan) this GenServer only operates through calling
  # to maintain backpressure and prevent overflowing the process
  # mailbox. Maybe there is a better way of achieving that end
  # which would free the scanning a processing processes to achieve
  # more throughput
  ##

  def start_link do
    # the index is named with the atom of its own module
    # enforcing it to be a singleton process
    # TODO(Dan) supervise this process to make sure it restarts
    # on errors
    GenServer.start_link(__MODULE__, [%{}], name: __MODULE__)
  end

  def inspect do
    GenServer.call(__MODULE__, :inspect)
  end

  def update({trigram, file}) do
    GenServer.call(__MODULE__, {:update, trigram, file})
  end

  def query(str) do
    GenServer.call(__MODULE__, {:query, str})
  end

  def init([state]) do
    ##
    # The state of this GenServer is a hash of trigrams to a set
    # of the files that they appear in
    ##
    {:ok, state}
  end

  def handle_call({:update, trigram, file}, _from, state) do
    {:reply, :ok, Map.update(state,
                             trigram,
                             MapSet.new([file]),
                             &MapSet.put(&1, file))}
  end

  def handle_call({:query, str}, _from, state) do
    {:reply,
      str
      |> to_trigrams
      |> query_with_trigrams(state)
      |> find_lines(str),
      state}
  end

  def handle_call(:inspect, _from, state) do
    {:reply, IO.inspect(state), state}
  end

  def to_trigrams(str) do
    if String.length(str) >= 3 do
      str
      |> String.graphemes
      |> Stream.chunk(3, 1)
      |> Stream.map(&Enum.join(&1))
      |> Enum.into(MapSet.new())
    else
      [str]
    end
  end

  def query_with_trigrams(trigrams, state) do
    Enum.reduce(trigrams, nil, fn
      # initial state
      trigram, nil -> Map.get(state, trigram, :not_found)

      # forward :not_found all the way to the end of the iteration
      _trigram, :not_found -> :not_found

      # iterative step
      trigram, currentset -> case Map.get(state, trigram, :not_found) do

        # forward :not_found
        :not_found -> :not_found

        # return the intersection of the set that the current trigram is found in
        # and the current available set of files
        # the string is not found if the intersection of these sets
        # has no length
        set -> MapSet.intersection(set, currentset)

      end
    end)
  end

  defp format_path(path), do: "\n---\n#{ANSI.magenta()}#{path}#{ANSI.default_color()}\n---\n"
  defp format_line({line, index, search}, accum) do
    "#{accum}#{ANSI.cyan()}#{index}#{ANSI.default_color()}: #{format_line(line, search)}\n"
  end
  defp format_line(line, search) do
    line
    |> String.replace(search,
                      "#{ANSI.green()}#{ANSI.default_color()}",
                      insert_replaced: String.length(ANSI.green()))
    |> String.trim()
  end

  def find_lines(:not_found, str), do: "No lines containing #{str} found"
  def find_lines(set, str) do
    ##
    # This will currently block the calling process until all of the files in
    # the set are read.
    # The Beam VM will preempt eventually and return if scheduling pressure
    # builds up (which is cool, but is a throughput tradeoff).
    # TODO(Dan) This file IO should probably be offloaded to a worker thread
    ##
    if MapSet.size(set) >= 0 do
      set
      |> Stream.map(fn path ->
        File.stream!(path, read_ahead: 100_000)
        |> Stream.with_index
        |> Stream.filter(fn {line, _index} -> String.contains?(line, str) end)
        |> Stream.map(fn {line, index} -> {line, index, str} end)
        |> Enum.reduce(format_path(path), &format_line/2)
      end)
      |> Enum.join
    else
      find_lines(:not_found, str)
    end
  end
end

