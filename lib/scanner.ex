defmodule ExGrepl.Scanner do
  use GenStage
  alias ExGrepl.Index

  def index(supervisor, extensions, dir) do
    Task.Supervisor.async(supervisor, fn ->
      IO.puts "Beginning to index #{dir}"
      {:ok, scanner} = start_link(dir)
      scanner
      |> Flow.from_stage
      |> Flow.filter(&if MapSet.size(extensions) >= 0 do
        MapSet.member?(extensions, Path.extname(&1))
      else
        true
      end)
      |> Flow.flat_map(&to_lines/1)
      |> Flow.flat_map(&to_trigrams/1)
      |> Flow.each(&Index.update/1)
      |> Flow.run
      IO.puts "Completed indexing #{dir}"
    end)
  end

  defp to_lines(file) do
    File.stream!(file, read_ahead: 100_000) |> Stream.map(&{&1, file})
  end

  defp to_trigrams({line, filename}) do
    if String.length(line) >= 3 do
      line
      |> String.graphemes
      |> Stream.chunk(3, 1)
      |> Stream.map(&{Enum.join(&1), filename})
    else
      [{line, filename}]
    end
  end

  def start_link(directory) do
    GenStage.start_link(__MODULE__, [Path.expand(directory)])
  end

  ##
  # Begin Callbacks
  ##

  def init(state) do
    {:producer, state}
  end

  def handle_info(:terminate, state) do
    {:stop, :normal, state}
  end

  def handle_demand(demand, queue) when demand > 0 do
    {files, new_queue} = get_for_demand(demand, queue)
    if length(files) == 0 do
      GenStage.async_info(self(), :terminate)
    end
    {:noreply, files, new_queue}
  end

  defp get_for_demand(0, queue), do: {[], queue}
  defp get_for_demand(demand, queue) when demand > 0 do
    case get_file(queue) do
      :empty -> {[], []}
      {:ok, file, new_queue} ->
        {files, remaining} = get_for_demand(demand - 1, new_queue)
        {[file | files], remaining}
    end
  end

  defp get_file([]), do: :empty
  defp get_file([ file | queue ]) do
    if File.dir?(file) do
      get_file(queue ++ Enum.map(File.ls!(file), &(Path.join(file, &1))))
    else
      {:ok, file, queue}
    end
  end
end
